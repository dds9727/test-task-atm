import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:test_task_atm/atm/view/atm_page.dart';
import 'dart:ui' as ui;

import 'package:test_task_atm/atm_observer.dart';

void main() {
  Bloc.observer = ATMObserver();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: Colors.transparent));
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.deepPurple,
        accentColor: Colors.deepPurple,
        backgroundColor: Color(0xFFF3F2FA),
        textSelectionTheme: TextSelectionThemeData(
          selectionColor: Colors.yellow.withOpacity(0.5),
          cursorColor: Colors.yellow,
          selectionHandleColor: Colors.yellow,
        ),
        textTheme: TextTheme(
          headline1: GoogleFonts.openSans(
            color: Colors.white,
            fontSize: 15,
            fontWeight: FontWeight.w400,
          ),
          bodyText1: GoogleFonts.openSans(
            color: Colors.white,
            fontSize: 30,
            fontWeight: FontWeight.w600,
          ),
          button: GoogleFonts.openSans(
            color: Colors.white,
            fontSize: 16,
            fontWeight: FontWeight.w500,
          ),
          subtitle1: GoogleFonts.openSans(
            color: Color(0xFFA3A2AC),
            fontSize: 13,
            fontWeight: FontWeight.w400,
          ),
          headline2: GoogleFonts.openSans(
            color: Color(0xFF3827B4),
            fontSize: 14,
            fontWeight: FontWeight.w600,
          ),
        ),
        buttonTheme: ButtonThemeData(
          buttonColor: Color(0xFFE61EAD),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
          ),
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 42),
        ),
      ),
      home: ATMPage(),
    );
  }
}