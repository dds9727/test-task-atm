class ATM {
  static GiveCashResult giveCash({required List<Banknote> banknotes, required double sum, int banknoteIndex = 0}) {
    List<Banknote> cash = [];
    Banknote _thisBanknote = banknotes[banknoteIndex];
    int thisBanknoteSuitableCount = (sum / _thisBanknote.value).floor();
    thisBanknoteSuitableCount = thisBanknoteSuitableCount > _thisBanknote.count ? _thisBanknote.count : thisBanknoteSuitableCount;
    if (thisBanknoteSuitableCount > 0) {
      cash.add(Banknote(value: _thisBanknote.value, count: thisBanknoteSuitableCount));
    }
    bool _success = false;
    if (sum > 0 && banknoteIndex + 1 < banknotes.length) {

      GiveCashResult _recursiveCall = giveCash(
        banknotes: banknotes,
        sum: sum - _thisBanknote.value * thisBanknoteSuitableCount,
        banknoteIndex: banknoteIndex + 1,
      );

      if (_recursiveCall.success) {
        cash.addAll(_recursiveCall.banknotes);
        _success = true;
      }
    }
    if (sum - thisBanknoteSuitableCount * _thisBanknote.value == 0) {
      _success = true;
    }
    return GiveCashResult(success: _success, banknotes: cash);
  }
}

class Banknote {
  int value = 0;
  int count = 0;

  Banknote({required this.value, this.count = 0});
}

class GiveCashResult {
  List<Banknote> banknotes;
  bool success;

  GiveCashResult({this.success = true, required this.banknotes});
}
