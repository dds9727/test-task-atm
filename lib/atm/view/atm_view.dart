import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:test_task_atm/atm/cubit/atm_cubit.dart';

const Color primaryLeft = Color(0xFF3827B4);
const Color primaryRight = Color(0xFF6C18A4);

class ATMView extends StatefulWidget {
  @override
  _ATMViewState createState() => _ATMViewState();
}

class _ATMViewState extends State<ATMView> {
  final TextEditingController numberController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        title: Container(
          child: Image.asset('assets/images/Logo.png'),
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: <Color>[primaryLeft, primaryRight],
            ),
          ),
        ),
      ),
      body: BlocBuilder<ATMCubit, ATMState>(
        builder: (context, state) {
          var remainingBanknotes = []..addAll(state.remainingBanknotes);
          remainingBanknotes.sort((a,b) => a.value > b.value ? 1 : 0);
          return LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) => CustomScrollView(
              slivers: [
                SliverList(
                  delegate: SliverChildListDelegate(
                    [
                      Container(
                        height: 180,
                        width: constraints.maxWidth,
                        color: Colors.white,
                        child: Stack(
                          children: [
                            Positioned(
                              top: 0,
                              left: -10,
                              height: 180,
                              width: constraints.maxWidth + 10,
                              child: Image.asset(
                                'assets/images/Background.png',
                                fit: BoxFit.fill,
                                height: 180,
                                width: constraints.maxWidth,
                              ),
                            ),
                            Container(
                              width: constraints.maxWidth,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  SizedBox(height: 25),
                                  Text(
                                    'Введите сумму',
                                    style: Theme.of(context).textTheme.headline1,
                                  ),
                                  Container(
                                    width: 250,
                                    child: TextField(
                                      controller: numberController,
                                      keyboardType: TextInputType.numberWithOptions(
                                        decimal: true,
                                        signed: false,
                                      ),
                                      decoration: InputDecoration(
                                        border: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                            width: 1,
                                            color: Colors.white.withOpacity(0.3),
                                          ),
                                        ),
                                        suffixText: 'руб',
                                        suffixStyle: Theme.of(context).textTheme.bodyText1,
                                        contentPadding: EdgeInsets.symmetric(vertical: 8, horizontal: 5),
                                      ),
                                      textAlign: TextAlign.center,
                                      style: Theme.of(context).textTheme.bodyText1,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        width: constraints.maxWidth,
                        padding: EdgeInsets.only(bottom: 25),
                        child: Center(
                          child: ATMButton(
                            text: 'Выдать сумму',
                            onPressed: () {
                              double sum = double.tryParse(numberController.text) ?? -1;
                              var _scaffold = ScaffoldMessenger.of(context);
                              if (sum > 0) {
                                _scaffold.hideCurrentSnackBar();
                                context.read<ATMCubit>().getCash(sum);
                              } else {
                                _scaffold.hideCurrentSnackBar();
                                _scaffold.showSnackBar(
                                  SnackBar(
                                    content: Text(
                                      'Пожалуйста, введите корректную сумму',
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                    backgroundColor: Color(0xFFFF5A5A),
                                    behavior: SnackBarBehavior.floating,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),
                                  ),
                                );
                              }
                            },
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                      (() {
                        if (state is ATMError) {
                          return ATMCard(
                            constraints: constraints,
                            child: Container(
                              height: 103.5,
                              child: Center(
                                child: Text(
                                  '${state.message}',
                                  style: GoogleFonts.openSans(
                                    color: Color(0xFFE61EAD),
                                    fontSize: 18,
                                    fontWeight: FontWeight.w400,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          );
                        } else if (state is ATMGivingCash) {
                          return ATMCard(
                            constraints: constraints,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Банкомат выдал следующие купюры',
                                  style: Theme.of(context).textTheme.subtitle1,
                                ),
                                SizedBox(height: 10),
                                Container(
                                  width: constraints.maxWidth,
                                  child: GridView.builder(
                                    physics: NeverScrollableScrollPhysics(),
                                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 2,
                                      childAspectRatio: (constraints.maxWidth / 2) / 28,
                                    ),
                                    shrinkWrap: true,
                                    itemBuilder: (BuildContext context, int index) => Container(
                                      height: 50,
                                      padding: EdgeInsets.symmetric(vertical: 3),
                                      child: Text(
                                        '${state.givenBanknotes[index].count} X ${state.givenBanknotes[index].value} рублей',
                                        style: Theme.of(context).textTheme.headline2,
                                      ),
                                    ),
                                    itemCount: state.givenBanknotes.length,
                                  ),
                                ),
                              ],
                            ),
                          );
                        } else {
                          return ATMCard(
                            constraints: constraints,
                            child: Container(
                              height: 103.5,
                              child: Center(
                                child: Text(
                                  'Пожалуйста, введите нужную Вам сумму',
                                  style: GoogleFonts.openSans(
                                    color: Color(0xFFE61EAD),
                                    fontSize: 18,
                                    fontWeight: FontWeight.w400,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          );
                        }
                      }()),
                      SizedBox(height: 10),
                      ATMCard(
                        constraints: constraints,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Баланс банкомата',
                              style: Theme.of(context).textTheme.subtitle1,
                            ),
                            SizedBox(height: 15),
                            Container(
                              width: constraints.maxWidth,
                              child: GridView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2,
                                  childAspectRatio: (constraints.maxWidth / 2) / 30,
                                ),
                                shrinkWrap: true,
                                itemBuilder: (BuildContext context, int index) => Container(
                                  height: 50,
                                  padding: EdgeInsets.symmetric(vertical: 3),
                                  child: Text(
                                    '${remainingBanknotes[index].count} X ${remainingBanknotes[index].value} рублей',
                                    style: Theme.of(context).textTheme.headline2,
                                  ),
                                ),
                                itemCount: remainingBanknotes.length,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 10),
                    ],
                  ),
                ),
                Footer(constraints: constraints),
              ],
            ),
          );
        },
      ),
    );
  }
}

class ATMCard extends StatelessWidget {
  final BoxConstraints constraints;
  final Widget? child;

  ATMCard({required this.constraints, this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: constraints.maxWidth,
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      padding: EdgeInsets.symmetric(vertical: 25, horizontal: 21),
      child: child,
    );
  }
}

class ATMButton extends StatelessWidget {
  final void Function() onPressed;
  final String? text;

  ATMButton({this.text, required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        primary: Color(0xFFE61EAD),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(50),
        ),
        padding: EdgeInsets.symmetric(vertical: 20, horizontal: 42),
      ),
      onPressed: onPressed,
      child: Text(
        '$text',
        style: GoogleFonts.openSans(
          fontSize: 16,
          fontWeight: FontWeight.w500,
        ),
      ),
    );
  }
}

class Footer extends StatelessWidget {
  final BoxConstraints constraints;

  Footer({required this.constraints});

  @override
  Widget build(BuildContext context) {
    return SliverFillRemaining(
      hasScrollBody: false,
      child: Container(
        color: Colors.white,
        child: Column(
          children: [
            Expanded(
              child: Container(),
            ),
            SizedBox(height: 10),
            Image.asset(
              'assets/images/Background_bottom.png',
              fit: BoxFit.fill,
              width: constraints.maxWidth,
              height: 100,
            ),
          ],
        ),
      ),
    );
  }
}
