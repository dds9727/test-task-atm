import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_task_atm/atm/cubit/atm_cubit.dart';
import 'atm_view.dart';

class ATMPage extends StatelessWidget {
  const ATMPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => ATMCubit(),
      child: ATMView(),
    );
  }
}
