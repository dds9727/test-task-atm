import 'package:bloc/bloc.dart';
import 'package:test_task_atm/atm/models/atm.dart';

class ATMCubit extends Cubit<ATMState> {
  ATMCubit()
      : super(
          ATMInitial(
            remainingBanknotes: [
              Banknote(value: 5000, count: 10),
              Banknote(value: 100, count: 50),
              Banknote(value: 200, count: 100),
              Banknote(value: 500, count: 5),
              Banknote(value: 1000, count: 10),
              Banknote(value: 2000, count: 100),
            ]..sort((a, b) => a.value < b.value ? 1 : 0),
          ),
        );

  void getCash(double sum) {
    GiveCashResult result = ATM.giveCash(banknotes: state.remainingBanknotes, sum: sum);
    if (result.success) {
      result.banknotes.forEach((outputBanknote) {
        final previousValueBanknotes = state.remainingBanknotes.firstWhere((element) => element.value == outputBanknote.value);
        previousValueBanknotes.count -= outputBanknote.count;
      });
      result.banknotes.sort((a,b) => a.value > b.value ? 1 : 0);
      emit(ATMGivingCash(remainingBanknotes: state.remainingBanknotes, givenBanknotes: result.banknotes));
    } else {
      emit(ATMError(remainingBanknotes: state.remainingBanknotes, message: 'Банкомат не может выдать запрашиваемую сумму'));
    }
  }
}

abstract class ATMState {
  final List<Banknote> remainingBanknotes = const [];
  const ATMState();
}

class ATMInitial extends ATMState {
  final List<Banknote> remainingBanknotes;

  const ATMInitial({required this.remainingBanknotes});
}

class ATMError extends ATMState {
  final List<Banknote> remainingBanknotes;
  final String message;

  const ATMError({this.message = '', required this.remainingBanknotes});
}

class ATMGivingCash extends ATMState {
  final List<Banknote> remainingBanknotes;
  final List<Banknote> givenBanknotes;

  const ATMGivingCash({required this.remainingBanknotes, required this.givenBanknotes});
}
